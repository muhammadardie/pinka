<?php

namespace App\services;

use App\Repositories\RecordLogRepository;
use App\Repositories\FeverRepository;
use App\Repositories\EmployeeRepository;

class ConditionService
{
    public function __construct(RecordLogRepository $recordLog, FeverRepository $fever, EmployeeRepository $employee)
    {
        $this->recordLog = $recordLog;
        $this->fever     = str_replace('.', ',', $fever->show(1)->temperature);
        $this->employee  = $employee;
    }

    /**
     * Count total normal and fever of employees by date
     * $period = period of date
     * @return array ['normal' => $totalNormal, 'fever' => $totalFever ]
     */
    public function getCondition($period)
    {
        $emps        = $this->employee->getModel()->pluck('UserID');
        $countFever  = 0;
        $countNormal = 0;

        foreach ($period as $date) {
            foreach ($emps as $emp) {
                $empFever = $this->recordLog
                               ->getModel()
                               ->whereRaw('DateValue(RecordTime) = #'. $date->format("Y/m/d") . '#')
                               ->where('UserID', $emp)
                               ->where('Temperature', '>=', $this->fever)
                               ->first();

                $empFever ? $countFever++ : $countNormal++;
            }
        }

        return ['normal' => $countNormal, 'fever' => $countFever];
    }

    /**
     * check is employee fever
     * @return true when employee fever / false when employee normal / null when there is no log
     */
    public function employeeFever($employeeId)
    {
        $latestLog = $this->recordLog
                        ->where(['UserID' => $employeeId])
                        ->orderBy('RecordTime', 'desc')
                        ->first();

        return $latestLog ? str_replace('.', ',', $latestLog->Temperature) >= $this->fever : $latestLog;
    }

    /**
     * check is temperature == fever
     * @return true when temperature same or more than fever limit
     */
    public function temperatureFever($temperature)
    {
        return str_replace('.', ',', $temperature) >= $this->fever;
    }
}
