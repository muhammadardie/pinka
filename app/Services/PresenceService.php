<?php

namespace App\services;

use App\Repositories\RecordLogRepository;
use App\Repositories\AbsenceRepository;

class PresenceService
{
    public function __construct(RecordLogRepository $recordLog, AbsenceRepository $absence)
    {
        $this->recordLog = $recordLog;
        $this->absence  = $absence;
    }

    public function getPresence($period)
    {
        $absencePresent = strtotime($this->absence->show(2)->EndTime);
        $absenceLate    = strtotime($this->absence->show(3)->EndTime);
        $absenceLeave   = strtotime($this->absence->show(4)->EndTime);

        $presences = ['present' => [], 'excuse' => [], 'late' => []];
        // loop days from last week until today
        foreach ($period as $day) {
            $logs = $this->recordLog
                        ->getModel()
                        ->whereRaw('DateValue(RecordTime) = #'. $day->format("Y/m/d") . '#')
                        ->where('UserID', '<>', '-1')
                        ->orderBy('UserID')
                        ->get();

            $countPresent = 0;
            $countLate    = 0;
            $countExcuse  = 0;
            $tempUser     = 0;
            // loop user record by day
            foreach ($logs as $keyLog =>  $log) {
                // if log still same user
                if ($tempUser != $log->UserID) {
                    // get all log by day and user id
                    $userLog = $this->recordLog
                                    ->getModel()
                                    ->whereRaw('DateValue(RecordTime) = #'. $day->format("Y/m/d") . '#')
                                    ->where('UserID', $log->UserID)
                                    ->orderBy('RecordTime')
                                    ->first();
                    if ($userLog) {
                        $tempUser = $log->UserID;
                        // present
                        if (\Helper::toUnixTime($userLog->RecordTime) <= $absencePresent) {
                            $countPresent++;
                        }
                        // excuse
                        elseif (\Helper::toUnixTime($userLog->RecordTime) >= $absenceLate) {
                            $countExcuse++;
                        }
                        // late
                        elseif (\Helper::toUnixTime($userLog->RecordTime) >= $absencePresent && \Helper::toUnixTime($userLog->RecordTime) <= $absenceLate) {
                            $countLate++;
                        }
                    }
                }
            }
            array_push($presences['present'], $countPresent);
            array_push($presences['excuse'], $countExcuse);
            array_push($presences['late'], $countLate);
        }

        return $presences;
    }

    /**
     * Get status presence by record time in record log
     * $time record time
     * return 'PRESENT' when record time not in any range from table absence
     * @return String 'PRESENT' / 'LATE' / 'LEAVES' / 'EXCUSE'
     */
    public function statusPresence($time)
    {
        $absence  = $this->absence->all();
        $timeOnly = date('H:i', strtotime($time));
        $strTime  = strtotime($timeOnly);
        $presence = 'PRESENT';
        foreach ($absence as $value) {
            $start = strtotime($value->StartTime);
            $end   = strtotime($value->EndTime);

            if ($strTime > $start && $strTime < $end) {
                return $value->Status;
            }
        }

        return $presence;
    }

    /**
     * Filter record log into absence log (only show present and leave log)
     * $recordLog record log
     * @return row filtered from record log
     */
    public function filterIntoAbsence($recordLog)
    {
        $presentTimeLimit = strtotime($this->absence->show(3)->EndTime);
        $collectRecord    = collect($recordLog);
        $collectAbsence   = collect([]);

        for ($i=0; count($collectRecord) > 0  ; $i++) {
            $first = $collectRecord->first();
            $sameLog = $collectRecord->where('LogDate', $first->LogDate)
                                     ->where('UserName', $first->UserName);
            // remove same log
            $collectRecord = $collectRecord->reject(function ($value, $key) use ($first) {
                return $first->LogDate == $value->LogDate && $first->UserName == $value->UserName;
            });

            // present log
            $present = $sameLog->filter(function ($value, $key) use ($presentTimeLimit) {
                return strtotime($value->LogTime) < $presentTimeLimit;
            });

            // leave log
            $leave = $sameLog->filter(function ($value, $key) use ($presentTimeLimit) {
                return strtotime($value->LogTime) > $presentTimeLimit;
            });

            if (count($present) > 0) {
                $firstPresent    = $present->sortBy('LogTime')->first();
                $colFirstPresent = collect($firstPresent);
                $merged          = $colFirstPresent->merge(["status" => $this->statusPresence($firstPresent->RecordTime)]);
                $collectAbsence->push($merged);
            }

            if (count($leave) > 0) {
                $lastLeave = $leave->sortByDesc('LogTime')->first();
                $colLastLeave = collect($lastLeave);

                if (count($present) == 0) {
                    $merged = $colLastLeave->merge(["status" => "EXCUSE"]);
                } else {
                    $merged = $colLastLeave->merge(["status" => $this->statusPresence($lastLeave->RecordTime)]);
                }

                $collectAbsence->push($merged);
            }
        }

        return $collectAbsence;
    }
}
