<?php

namespace App\services;

use App\Repositories\RecordLogRepository;
use App\Repositories\FeverRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\DepartmentRepository;
use App\Services\PresenceService;

class ScanService
{
    public function __construct(
        RecordLogRepository $recordLog,
        FeverRepository $fever,
        EmployeeRepository $employee,
        DepartmentRepository $department,
        PresenceService $presence
    ) {
        $this->recordLog  = $recordLog;
        $this->fever      = $fever;
        $this->employee   = $employee;
        $this->department = $department;
        $this->presence   = $presence;
    }

    /**
     * return id of employee scanned by date period
     * $period = period of date
     * @return array of id employee
     */
    public function empScanned($period=null)
    {
        $empScanned = $this->recordLog
                           ->getModel()
                           ->select('UserID')
                           ->whereRaw('DateValue(RecordTime) = Date()')
                           ->where('UserID', '<>', '-1')
                           ->distinct()
                           ->pluck('UserID');

        return $empScanned;
    }

    /**
     * return time latest scan
     * @return time in indo
     */
    public function lastTimeScan($employeeId)
    {
        $record = $this->recordLog
                        ->where(['UserID' => $employeeId])
                        ->orderBy('RecordTime', 'desc')
                        ->first();

        return $record ? \Helper::tglIndo($record->RecordTime) : '-';
    }

    /**
     * return latest record scan if it was valid
     */
    public function getLatestScan()
    {
        $latestScan      = $this->recordLog->getModel()->orderBy('RecordTime', 'desc')->first();
        $filteredScan    = $this->filterScan($latestScan);
        $constructedScan = $this->constructScan($latestScan);

        return $constructedScan;
    }

    /**
     * if latest scan more than 3 minute ago then return empty array else latest scan
     */
    public function filterScan($latestScan)
    {
        if (is_null($latestScan)) {
            return [];
        }

        $threeMinAgo = date("Y-m-d H:i:s", strtotime("-3 minutes"));
        $scan        = $latestScan->RecordTime < $threeMinAgo ? [] : $latestScan;

        return $scan;
    }

    /**
     * add new properties to latest scan like username , etc
     */
    public function constructScan($latestScan)
    {
        if (empty($latestScan)) {
            return $latestScan;
        }

        $latestScan->username   = 'visitor';
        $latestScan->department = 'visitor';

        $tempLimit = $this->fever->show(1)->temperature;

        // employee
        if ($latestScan->UserID != -1) {
            $userInfo  = $this->employee->where(['UserID' => $latestScan->UserID])->first();
            $userGroup = null;

            if ($userInfo) {
                $userGroup = $this->department->show($userInfo->GroupID);
                $latestScan->username   = $userInfo->UserName;
            }

            if ($userGroup) {
                $latestScan->department = $userGroup->GroupName;
            }
        }

        $latestScan->time      = date('H:i', strtotime($latestScan->RecordTime));
        $latestScan->PicPath   = str_replace(config('facemanager.path'), url(''). '/', $latestScan->PicPath);
        $latestScan->condition = $latestScan->Temperature >= $tempLimit ? 'fever' : 'normal';
        $latestScan->presence  = $this->presence->statusPresence($latestScan->time);

        return $latestScan;
    }
}
