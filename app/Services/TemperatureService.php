<?php

namespace App\services;

use App\Repositories\{ RecordLogRepository };

class TemperatureService
{
    public function __construct(RecordLogRepository $recordLog)
    {
        $this->recordLog = $recordLog;
    }

    public function getTemperature($period)
    {
        $types = ['min', 'avg', 'max'];

        foreach ($types as $type) {
            $temperatures[$type] = [];

            foreach ($period as $day) {
                $temperature = $this->recordLog
                                ->getModel()
                                ->whereRaw('DateValue(RecordTime) = #'. $day->format("Y/m/d") . '#')
                                ->{$type}('Temperature');
                $temperature = $temperature ? (string) round($temperature, 1) : 0;

                array_push($temperatures[$type], $temperature);
            }
        }

        return $temperatures;
    }
}
