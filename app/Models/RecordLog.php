<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecordLog extends Model
{
    protected $connection = 'odbc';
    protected $table      = 'RecordLog';
    protected $primaryKey = 'ID';
    protected $guarded    = [];
    public $timestamps    = false;
    public $incrementing  = false;
}
