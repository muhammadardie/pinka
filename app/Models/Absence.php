<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    protected $connection = 'odbc';
    protected $table      = 'Absence';
    protected $primaryKey = 'ID';
    protected $guarded    = [];
    public $timestamps    = false;
    public $incrementing  = false;
}
