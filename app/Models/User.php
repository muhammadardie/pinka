<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'odbc';
    protected $table      = 'Users';
    protected $primaryKey = 'ID';
    protected $guarded    = [];
    public $incrementing = false;
    public $timestamps    = false;

    public function employee()
    {
        return $this->hasOne(Employee::class, 'UserID', 'UserId');
    }
}
