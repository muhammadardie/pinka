<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $connection = 'odbc';
    protected $table      = 'UserInfo';
    protected $primaryKey = 'ID';
    protected $guarded    = [];
    public $timestamps    = false;

    public function department()
    {
        return $this->hasOne(Department::class, 'ID', 'GroupID');
    }
}
