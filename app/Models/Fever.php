<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fever extends Model
{
    protected $connection = 'odbc';
    protected $table      = 'Fever';
    protected $primaryKey = 'FeverID';
    protected $guarded    = [];
    public $timestamps    = false;
    public $incrementing  = false;
}
