<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $connection = 'odbc';
    protected $table      = 'UserGroup';
    protected $primaryKey = 'ID';
    protected $guarded    = [];
    public $timestamps    = false;
}
