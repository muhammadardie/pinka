<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ID = $this->route('user_account') ?? 0;

        return [
            'UserID'   => [ $ID ? 'nullable' : 'required' ],
            'email'    => ['required','email', 'max:150', 'unique:Users,email,'.$ID.',ID'],
            'password' => [ $ID ? 'nullable' : 'required','string','min:6','confirmed']
         ];
    }
}
