<?php

namespace App\Http\Controllers\Monitoring;

use App\Http\Controllers\Controller;
use App\Repositories\FeverRepository;
use App\Services\ScanService;

class LiveScanController extends Controller
{
    public function __construct(FeverRepository $fever, ScanService $scan)
    {
        $this->scan  = $scan;
        $this->fever = $fever;
    }

    public function index()
    {
        $temperature = $this->fever->show(1)->value('temperature');

        return view('monitoring.live_scan_index', compact('temperature'));
    }

    public function monitor()
    {
        return $this->scan->getLatestScan();
    }
}
