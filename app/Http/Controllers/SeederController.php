<?php

namespace App\Http\Controllers;

use App\Repositories\RecordLogRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\FeverRepository;
use App\Repositories\AbsenceRepository;

class SeederController extends Controller
{
    public function __construct(RecordLogRepository $recordLog, EmployeeRepository $employee, FeverRepository $fever, AbsenceRepository $absence)
    {
        $this->recordLog = $recordLog;
        $this->employee  = $employee;
        $this->fever     = $fever;
        $this->absence   = $absence;
    }

    /**
     * Generate dummy data scan for all registered employee
     * 1 employee 2x scan with random temperature
     * first scan between 06:00 - 13:00
     * second scan between 12:00 - 18:00
     * @return 'success'
     */
    public function dummyScan()
    {
        $employees = $this->employee->all();
        $pathImg   = Config('facemanager.path') . Config('facemanager.folder') . '\\Record\\' . Config('facemanager.device') . '\\';
        $minTemp   = 36;
        $maxTemp   = 37;

        // loop 2x for present and leave
        for ($i=0; $i < 2 ; $i++) {


            // loop all employee
            foreach ($employees as $key => $employee) {
                $attendance = $i == 0 ? 'present' : 'leave';
                $randomTime = $this->randomTime($attendance);
                $randomTemp = $this->randomTemp($minTemp, $maxTemp);
                $scan = [
                    'UserID'      => $employee->UserID,
                    'RecordTime'  => $randomTime,
                    'Temperature' => $randomTemp,
                    'PicPath'     => $pathImg.$employee->UserID.'.jpg'
                ];
                $store = $this->recordLog->store($scan);
            }
        }

        return 'success';
    }

    public function randomTime($attendance)
    {
        if ($attendance == 'present') {
            $start = date('Y-m-d 06:00:00');
            $end   = date('Y-m-d 12:00:00');
        } elseif ($attendance == 'leave') {
            $start = date('Y-m-d 12:00:00');
            $end   = date('Y-m-d 06:00:00');
        }

        $min_epoch = strtotime($start);
        $max_epoch = strtotime($end);

        $rand_epoch = rand($min_epoch, $max_epoch);

        return date('Y-m-d H:i:s', $rand_epoch);
    }

    public function randomTemp($minTemp, $maxTemp)
    {
        $randomTemp = rand($minTemp*10, $maxTemp*10) / 10;
        $randomTemp = str_replace('.', ',', $randomTemp);

        return $randomTemp;
    }
}
