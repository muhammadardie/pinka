<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\EmployeeRepository;
use App\Repositories\RecordLogRepository;
use App\Services\ConditionService;
use Yajra\Datatables\Datatables;

class ScanningReportController extends Controller
{
    public function __construct(EmployeeRepository $employee, RecordLogRepository $recordLog, ConditionService $condition)
    {
        $this->employee  = $employee;
        $this->recordLog = $recordLog;
        $this->condition = $condition;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = $this->employee->getModel()->pluck('UserName', 'UserID');

        return view('report.scanning_report_index', compact('employees'));
    }

    /**
    * Showing list bank by datatable
    * @param $request ajax
    * @return json
    */
    public function ajaxDatatable(Request $request)
    {
        $condition = $this->condition;
        $recordLog = \DB::select(
            \DB::raw(
                'SELECT RecordLog.ID AS ID, RecordLog.RecordTime, UserInfo.UserName, UserGroup.GroupName AS department, RecordLog.Temperature 
                 FROM 
                 (RecordLog LEFT JOIN UserInfo ON UserInfo.UserID = RecordLog.UserID) 
                 LEFT JOIN UserGroup ON UserInfo.GroupID = UserGroup.ID
                 ORDER BY RecordLog.RecordTime DESC;
                '
            )
        );

        return Datatables::of($recordLog)
                ->filter(function ($instance) use ($request) {
                    if ($request->startdate != '') {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            $startDate  = new \DateTime($request->get('startdate'));
                            $recordTime = \DateTime::createFromFormat('d-m-Y H:i:s', $row['RecordTime']);
                            $recordTime->setTime(0, 0);

                            return $recordTime >= $startDate ? true : false;
                        });
                    }

                    if ($request->enddate != '') {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            $endDate    = new \DateTime($request->get('enddate'));
                            $recordTime = \DateTime::createFromFormat('d-m-Y H:i:s', $row['RecordTime']);
                            $recordTime->setTime(0, 0);

                            return $recordTime <= $endDate ? true : false;
                        });
                    }

                    if ($request->person != 'all') {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            if ($request->get('person') == 'Employee') {
                                return $row['UserName'] != 'Visitor' ? true : false;
                            } else {
                                return $row['UserName'] == $request->get('person') ? true : false;
                            }
                        });
                    }

                    if ($request->condition != 'all') {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return \Str::contains($row['conditions'], $request->get('condition')) ? true : false;
                        });
                    }
                })
                ->addColumn('RecordTime', function ($recordLog) {
                    return date('d-m-Y H:i:s', strtotime($recordLog->RecordTime));
                })
                ->addColumn('UserName', function ($recordLog) {
                    return $recordLog->UserName ?? 'Visitor';
                })
                ->addColumn('department', function ($recordLog) {
                    return $recordLog->department ?? 'Visitor';
                })
                ->addColumn('conditions', function ($recordLog) use ($condition) {
                    $status = $condition->temperatureFever($recordLog->Temperature) ?
                                '<label class="badge badge-danger">Fever</label>' :
                                '<label class="badge badge-success">Normal</label>';

                    return $status;
                })
                ->addColumn('Temperature', function ($recordLog) {
                    return $recordLog->Temperature. '&#8451;';
                })
                ->rawColumns(['conditions', 'Temperature']) // to html
                ->make(true);
    }
}
