<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\EmployeeRepository;
use App\Repositories\AbsenceRepository;
use App\Services\PresenceService;
use Yajra\Datatables\Datatables;

class AbsenceReportController extends Controller
{
    public function __construct(EmployeeRepository $employee, AbsenceRepository $absence, PresenceService $presence)
    {
        $this->employee = $employee;
        $this->absence  = $absence;
        $this->presence = $presence;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = $this->employee->getModel()->pluck('UserName', 'UserID');

        return view('report.absence_report_index', compact('employees'));
    }

    /**
    * Showing list bank by datatable
    * @param $request ajax
    * @return json
    */
    public function ajaxDatatable(Request $request)
    {
        $presentTimeLimit = strtotime($this->absence->show(3)->EndTime);

        $recordLog = \DB::select(
            \DB::raw(
                "SELECT RecordLog.ID AS ID, RecordLog.RecordTime, UserInfo.UserName, UserGroup.GroupName AS department, RecordLog.Temperature, Year(RecordTime)&'-'&Month(RecordTime)&'-'&Day(RecordTime) AS LogDate, Hour(RecordTime)&':'&Minute(RecordTime) AS LogTime
                 FROM 
                 (RecordLog INNER JOIN UserInfo ON UserInfo.UserID = RecordLog.UserID) 
                 INNER JOIN UserGroup ON UserInfo.GroupID = UserGroup.ID
                 ORDER BY RecordLog.RecordTime DESC;
                "
            )
        );

        $absences = $this->presence->filterIntoAbsence($recordLog);

        return Datatables::of($absences)
                ->filter(function ($instance) use ($request, $presentTimeLimit) {
                    if ($request->startdate != '') {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            $startDate  = new \DateTime($request->get('startdate'));
                            $recordTime = \DateTime::createFromFormat('d-m-Y H:i:s', $row['RecordTime']);
                            $recordTime->setTime(0, 0);

                            return $recordTime >= $startDate ? true : false;
                        });
                    }

                    if ($request->enddate != '') {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            $endDate    = new \DateTime($request->get('enddate'));
                            $recordTime = \DateTime::createFromFormat('d-m-Y H:i:s', $row['RecordTime']);
                            $recordTime->setTime(0, 0);

                            return $recordTime <= $endDate ? true : false;
                        });
                    }

                    if ($request->employee != 'all') {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return $row['UserName'] == $request->get('employee') ? true : false;
                        });
                    }

                    if ($request->absence != 'all') {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request, $presentTimeLimit) {
                            $recordTime = strtotime(date('H:i', strtotime($row['RecordTime'])));

                            if ($request->absence == 'present') {
                                return $recordTime < $presentTimeLimit;
                            } elseif ($request->absence == 'leave') {
                                return $recordTime > $presentTimeLimit;
                            } else {
                                return true;
                            }
                        });
                    }
                })
                ->addColumn('RecordTime', function ($recordLog) {
                    return date('d-m-Y H:i:s', strtotime($recordLog['RecordTime']));
                })
                ->addColumn('UserName', function ($recordLog) {
                    return $recordLog['UserName'] ?? 'Visitor';
                })
                ->addColumn('department', function ($recordLog) {
                    return $recordLog['department'] ?? 'Visitor';
                })
                ->addColumn('Temperature', function ($recordLog) {
                    return $recordLog['Temperature']. '&#8451;';
                })
                ->addColumn('absence', function ($recordLog) {
                    $badge = 'info';

                    if ($recordLog['status'] == 'LATE') {
                        $badge = 'danger';
                    } elseif ($recordLog['status'] == 'PRESENT') {
                        $badge = 'success';
                    } elseif ($recordLog['status'] == 'EXCUSE') {
                        $badge = 'warning';
                    }

                    return '<label class="badge badge-'. $badge .'">' .$recordLog['status']. '</label>';
                })
                ->rawColumns(['absence', 'Temperature']) // to html
                ->make(true);
    }
}
