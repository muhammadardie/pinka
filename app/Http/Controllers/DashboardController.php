<?php

namespace App\Http\Controllers;

use App\Repositories\EmployeeRepository;
use App\Services\TemperatureService;
use App\Services\PresenceService;
use App\Services\ConditionService;
use App\Services\ScanService;

class DashboardController extends Controller
{
    public function __construct(EmployeeRepository $employee, TemperatureService $temperature, PresenceService $presence, ConditionService $condition, ScanService $scan)
    {
        $this->employee    = $employee;
        $this->temperature = $temperature;
        $this->presence    = $presence;
        $this->condition   = $condition;
        $this->scan        = $scan;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        list($lastWeekdays, $lastWeekRange) = $this->createRangeDate('-6');
        list($today, $todayRange) = $this->createRangeDate('0');
        $temperature             = $this->temperature->getTemperature($lastWeekRange);
        $presence                = $this->presence->getPresence($lastWeekRange);
        $conditionEmpToday       = $this->condition->getCondition($todayRange);
        $absenceToday            = $this->presence->getPresence($todayRange);
        $totalEmp                = $this->employee->getModel()->count(); // total employee
        $totalScanToday = count($this->scan->empScanned());

        return view('dashboard', compact('lastWeekdays', 'temperature', 'totalEmp', 'totalScanToday', 'presence', 'conditionEmpToday', 'absenceToday'));
    }

    /* range date between $range until today */
    public function createRangeDate($range)
    {
        $rangeDate = [];
        $today     = new \DateTime(date('Y-m-d'));
        $today     = $today->modify('+1 day'); // make range add today
        $weekAgo   = new \DateTime(date('Y-m-d', strtotime(date('Y-m-d'). $range." days")));
        $interval  = new \DateInterval('P1D');
        $period = new \DatePeriod(
            $weekAgo,
            $interval,
            $today
        );

        foreach ($period as $value) {
            array_push($rangeDate, \Helper::tglIndo($value->format('Y-m-d')));
        };
        /* end range date */

        return [$rangeDate,$period];
    }
}
