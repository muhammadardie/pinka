<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\{ AbsenceRepository };
use App\Http\Requests\AbsenceRequest;
use Yajra\Datatables\Datatables;

class AbsenceController extends Controller
{
    protected $absence;

    public function __construct(AbsenceRepository $absence)
    {
        $this->absence = $absence;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.absence_index');
    }

    public function create()
    {
        return view('master.absence_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AbsenceRequest $request)
    {
        $store = $this->absence->store($request->all());

        return redirect()->route('absence.index')->with(\Helper::alertStatus('store', $store));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int
     * @return \Illuminate\Http\Response
     */
    public function update(AbsenceRequest $request, $AbsenceID)
    {
        $update = $this->absence->update($request->all(), $AbsenceID);

        return redirect()->route('absence.index')->with(\Helper::alertStatus('update', $update));
    }

    /**
     * Display the specified resource.
     *
     * @param  int
     * @return \Illuminate\Http\Response
     */
    public function show($AbsenceID)
    {
        $absence = $this->absence->show($AbsenceID);

        return view('master.absence_show', compact('absence'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int
     * @return \Illuminate\Http\Response
     */
    public function edit($AbsenceID)
    {
        $absence  = $this->absence->show($AbsenceID);

        return view('master.absence_edit', compact('absence'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->absence->delete($id);
    }

    /**
    * Showing list bank by datatable
    * @param $request ajax
    * @return json
    */
    public function ajaxDatatable(Request $request)
    {
        $absence = $this->absence->all();

        return Datatables::of($absence)
                ->addColumn('action', function ($absence) {
                    return '
                                <a class="datatable-buttons" href="' .route("absence.edit", $absence->ID). '" title="Edit user">
                                    <i class="mdi mdi-pencil-box-outline icon-md text-primary"></i>
                                </a>&nbsp;
                                
                            ';
                })
                ->rawColumns(['action'])
                ->make(true);
    }
}
