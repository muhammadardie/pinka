<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\{ FeverRepository };
use App\Http\Requests\FeverRequest;

class FeverController extends Controller
{
    protected $fever;

    public function __construct(FeverRepository $fever)
    {
        $this->fever = $fever;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fever = $this->fever->show(1);

        return view('master.fever_index', compact('fever'));
    }

    public function update(FeverRequest $request, $feverID)
    {
        $request['temperature'] = str_replace('.', ',', $request->temperature);
        $update = $this->fever->update($request->all(), $feverID);

        return redirect()->route('fever.index')->with(\Helper::alertStatus('update', $update));
    }
}
