<?php

namespace App\Http\Controllers\UserManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\RecordLogRepository;
use App\Services\ConditionService;
use App\Services\ScanService;
use Yajra\Datatables\Datatables;

class EmployeeController extends Controller
{
    public function __construct(ScanService $scan, ConditionService $condition)
    {
        $this->scan = $scan;
        $this->condition = $condition;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user_management.employee_index');
    }

    /**
    * Showing list bank by datatable
    * @param $request ajax
    * @return json
    */
    public function ajaxDatatable(Request $request)
    {
        $condition = $this->condition;
        $scan      = $this->scan;
        $employee = \DB::select(
            \DB::raw(
                'SELECT UserInfo.UserID, UserInfo.UserName, UserGroup.GroupName AS Department
                 FROM UserInfo 
                 INNER JOIN UserGroup ON UserInfo.GroupID = UserGroup.ID
                 ORDER BY UserName;
                '
            )
        );

        return Datatables::of($employee)
                ->addColumn('LastScan', function ($employee) use ($scan) {
                    return $scan->lastTimeScan($employee->UserID);
                })
                ->addColumn('Condition', function ($employee) use ($condition) {
                    $conditionText = '-';
                    $employeeFever = $condition->employeeFever($employee->UserID);

                    if ($employeeFever === true) {
                        return '<label class="badge badge-danger">Fever</label>';
                    }
                    if ($employeeFever === false) {
                        return '<label class="badge badge-success">Normal</label>';
                    }

                    return '<label class="badge badge-info">No data</label>';
                })
                ->rawColumns(['Condition']) // to html
                ->make(true);
    }
}
