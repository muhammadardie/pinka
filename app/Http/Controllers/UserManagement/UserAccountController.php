<?php

namespace App\Http\Controllers\UserManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\EmployeeRepository;
use App\Repositories\UserAccountRepository;
use App\Http\Requests\UserAccountRequest;
use Yajra\Datatables\Datatables;

class UserAccountController extends Controller
{
    public function __construct(UserAccountRepository $userAccount, EmployeeRepository $employee)
    {
        $this->employee    = $employee;
        $this->userAccount = $userAccount;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user_management.user_account_index');
    }

    public function create()
    {
        $userAccountIds = $this->userAccount->getModel()->pluck('UserId');
        $employees = $this->employee->getModel()->whereNotIn('UserID', $userAccountIds)->pluck('UserName', 'UserID');

        return view('user_management.user_account_create', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserAccountRequest $request)
    {
        $request['password']   = bcrypt($request['password']);
        $request['username']   = $this->employee->where(['UserID' => $request->UserID])->value('UserName');
        $request['created_at'] = date('Y-m-d H:i:s');
        $request['updated_at'] = date('Y-m-d H:i:s');

        $store = $this->userAccount->store($request->except(['password_confirmation']));

        return redirect()->route('user-account.index')->with(\Helper::alertStatus('store', $store));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int
     * @return \Illuminate\Http\Response
     */
    public function update(UserAccountRequest $request, $userId)
    {
        if ($request['password'] == '') {
            unset($request['password']);
        } else {
            $request['password'] = bcrypt($request['password']);
        }

        $update = $this->userAccount->update($request->except(['password_confirmation']), $userId);

        return redirect()->route('user-account.index')->with(\Helper::alertStatus('update', $update));
    }

    /**
     * Display the specified resource.
     *
     * @param  int
     * @return \Illuminate\Http\Response
     */
    public function show($userId)
    {
        $user = $this->userAccount->show($userId);

        return view('user_management.user_account_show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int
     * @return \Illuminate\Http\Response
     */
    public function edit($userId)
    {
        $user  = $this->userAccount->show($userId);

        return view('user_management.user_account_edit', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->userAccount->delete($id);
    }

    /**
    * Showing list bank by datatable
    * @param $request ajax
    * @return json
    */
    public function ajaxDatatable(Request $request)
    {
        $userAccount = $this->userAccount->all();

        return Datatables::of($userAccount)
                ->addColumn('created_at', function ($userAccount) {
                    return \Helper::tglIndo($userAccount->created_at);
                })
                ->addColumn('updated_at', function ($userAccount) {
                    return \Helper::tglIndo($userAccount->updated_at);
                })
                ->addColumn('action', function ($userAccount) {
                    return '
                                <a class="datatable-buttons" href="' .route("user-account.edit", $userAccount->ID). '" title="Edit user">
                                    <i class="mdi mdi-pencil-box-outline icon-md text-primary"></i>
                                </a>&nbsp;
                                <a class="datatable-buttons btn-delete-datatable" href="' .route("user-account.destroy", $userAccount->ID). '" title="Delete user">
                                    <i class="mdi mdi-delete icon-md text-primary"></i>
                                </a>
                            ';
                })
                ->rawColumns(['action'])
                ->make(true);
    }
}
