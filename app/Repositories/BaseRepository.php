<?php

namespace App\Repositories;

use App\Traits\{ EloquentTrait };

abstract class BaseRepository
{
    use EloquentTrait;
}
