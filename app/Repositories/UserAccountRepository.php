<?php

namespace App\Repositories;

use App\Models\User;

class UserAccountRepository extends BaseRepository
{
    public function __construct(User $userAccount)
    {
        $this->model = $userAccount;
    }
}
