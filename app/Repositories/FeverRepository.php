<?php

namespace App\Repositories;

use App\Models\Fever;

class FeverRepository extends BaseRepository
{
    public function __construct(Fever $fever)
    {
        $this->model = $fever;
    }
}
