<?php

namespace App\Repositories;

use App\Models\Absence;

class AbsenceRepository extends BaseRepository
{
    public function __construct(Absence $absence)
    {
        $this->model = $absence;
    }
}
