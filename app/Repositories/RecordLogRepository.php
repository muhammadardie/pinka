<?php

namespace App\Repositories;

use App\Models\RecordLog;

class RecordLogRepository extends BaseRepository
{
    public function __construct(RecordLog $recordLog)
    {
        $this->model = $recordLog;
    }
}
