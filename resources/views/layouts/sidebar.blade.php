<!-- partial:./partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
        <a class="sidebar-brand brand-logo logo-style" href="{{ url('') }}">
            <img src="{{ asset('assets/images/pinka.png') }}" />
        </a>
        <a class="sidebar-brand brand-logo-mini logo-style" href="{{ url('') }}">
            <img src="{{ asset('assets/images/p.png') }}" />
        </a>
    </div>
    <ul class="nav">
        <li class="nav-item profile">
            <div class="profile-desc">
                <div class="profile-pic">
                    <div class="count-indicator">
                        @php
                            $img = Auth::user()->employee->UserID;
                            $path = Config('facemanager.folder') . '/facedata/' . $img . '.jpg';
                        @endphp
                        <img class="img-xs rounded-circle" src="{{ asset($path) }}" alt="" />
                    </div>
                    <div class="profile-name">
                        <h5 class="mb-0 font-weight-normal">{{ \Auth::user()->employee->UserName }}</h5>
                        <span> {{ \Auth::user()->employee->department->GroupName }} </span>
                    </div>
                </div>
            </div>
        </li>
        <li class="nav-item nav-category">
            <span class="nav-link">Navigation</span>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" href="{{ route('dashboard') }}">
                <span class="menu-icon">
                    <i class="mdi mdi-speedometer"></i>
                </span>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#user-management" aria-expanded="false" aria-controls="user-management">
                <span class="menu-icon">
                    <i class="mdi mdi-account-key"></i>
                </span>
                <span class="menu-title">User Management</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="user-management">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('employee.index') }}">
                            Employee
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link-user-account" href="{{ route('user-account.index') }}">
                            User Account
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#master" aria-expanded="false" aria-controls="master">
                <span class="menu-icon">
                    <i class="mdi mdi-wrench"></i>
                </span>
                <span class="menu-title">Master</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="master">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('fever.index') }}">Fever</a></li>
                    <li class="nav-item"><a class="nav-link link-absence" href="{{ route('absence.index') }}">Absence</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#monitoring" aria-expanded="false" aria-controls="monitoring">
                <span class="menu-icon">
                    <i class="mdi mdi-monitor"></i>
                </span>
                <span class="menu-title">Monitoring</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="monitoring">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('live-scan') }}">
                            Live Scan
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#history" aria-expanded="false" aria-controls="history">
                <span class="menu-icon">
                    <i class="mdi mdi-history"></i>
                </span>
                <span class="menu-title">Report</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="history">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('scanning-report.index') }}">Scanning Report</a></li>
                </ul>
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('absence-report.index') }}">Absence Report</a></li>
                </ul>
            </div>
        </li>
    </ul>
</nav>
