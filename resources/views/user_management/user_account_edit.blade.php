@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">User Account</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <p class="card-description">Edit User Account</p>
                    <form class="forms-sample" id="form-edit-user-account" action="{{ route('user-account.update', $user->ID) }}" method="post">
                        @csrf
                        {{ method_field('PATCH') }}
                        @include('partials.form-input', [
                          'title'       => __('Email'),
                          'type'        => 'email',
                          'name'        => 'email',
                          'value'       => $user->email,
                          'required'    => true
                        ])
                        @include('partials.form-input', [
                          'title'       => __('Password'),
                          'type'        => 'password',
                          'name'        => 'password'
                        ])
                        @include('partials.form-input', [
                          'title'       => __('Confirm Password'),
                          'type'        => 'password',
                          'name'        => 'password_confirmation'
                        ])
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <a href="{{ route('user-account.index') }}" class="btn btn-dark">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{!! JsValidator::formRequest('App\Http\Requests\UserAccountRequest', '#form-edit-user-account') !!}
<script>
    $(document).ready(function() {
        // set navigation link active
        $('.nav-link[href="#user-management"]').parent().addClass('active');
        $('.link-user-account').addClass('active')
        $('.link-user-account').parent().parent().parent().addClass('show')

    });
</script>
@endsection