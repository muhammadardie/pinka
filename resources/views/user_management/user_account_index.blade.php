@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">User Account</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('partials.alert')
                    <div>
                        <a class="btn btn-primary btn-icon-text" href="{{ route('user-account.create') }}">
                            <i class="mdi mdi-note-plus btn-icon-prepend"></i> 
                            Add User 
                        </a>
                    </div>
                    <br />
                    <div class="table-responsive">
                      <table class="table table-bordered" id="table">
                        <thead>
                          <tr>
                            <th> No </th>
                            <th> Username </th>
                            <th> Email </th>
                            <th> Created At </th>
                            <th> Updated At </th>
                            <th> Action </th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        // datatable
        var table = $('#table').DataTable({
         processing: true,
         serverSide: true,
         searching: false,
         ordering: false,
         ajax: '{{ route('user-account.ajaxDatatable') }}',
         columns: [
             {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                } 
             },
             {data: 'username', name: 'users.username'},
             {data: 'email', name: 'users.email'},
             {data: 'created_at', name: 'users.created_at'},
             {data: 'updated_at', name: 'users.updated_at'},
             { data: 'action', className: 'text-center', width: "20%" }
         ],
         "drawCallback": function(settings) {
         
          },            
             pageLength: 10,
         });
    });
</script>
@include('partials.datatable-delete', ['text' => 'User', 'table' => '#table'])
@endsection