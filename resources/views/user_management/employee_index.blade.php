@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">Employee</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered" id="table">
                        <thead>
                          <tr>
                            <th> No </th>
                            <th> Username </th>
                            <th> Department </th>
                            <th> Last Scan </th>
                            <th> Condition </th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        // datatable
        var table = $('#table').DataTable({
         processing: true,
         serverSide: true,
         searching: false,
         ordering: false,
         ajax: '{{ route('employee.ajaxDatatable') }}',
         columns: [
             {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                } 
             },
             {data: 'UserName', name: 'UserInfo.UserName'},
             {data: 'Department', name: 'UserGroup.GroupName'},
             {data: 'LastScan'},
             {data: 'Condition'},
         ],
         "drawCallback": function(settings) {
         
          },            
             pageLength: 10,
         });
    });
</script>
@endsection