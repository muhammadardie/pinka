@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">Fever</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="alert alert-success mt-4 d-flex align-items-center" role="alert" style="margin:20px 30px !important">
                    <i class="ti-info-alt"></i>
                    <p>
                        Set the temperature limit for fever. for example if we set the temperature 37.5&#8451; then below that temperature condition will be "normal" but more than 37.5&#8451; condition will be "fever"
                    </p>
                </div>
                @include('partials.alert')
                <div class="card-body">
                    <form class="forms-sample" id="form-edit-fever" action="{{ route('fever.update', $fever->FeverID) }}" method="post">
                        @csrf
                        {{ method_field('PATCH') }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Temperature</label>
                            <div class="input-group col-sm-6">
                                <input type="number" class="form-control" name="temperature" value="{{ $fever->temperature }}" />
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        &#8451;
                                    </span>
                                </div>
                            </div>
                        </div>
                        <br />
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{!! JsValidator::formRequest('App\Http\Requests\FeverRequest', '#form-edit-fever') !!}
@endsection