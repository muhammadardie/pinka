@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="./assets/vendors/pickadate/themes/default.css">
<link rel="stylesheet" href="./assets/vendors/pickadate/themes/default.time.css">
<script src="./assets/vendors/pickadate/picker.js"></script>
<script src="./assets/vendors/pickadate/picker.time.js"></script>
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">Absence</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <p class="card-description">Edit Absence</p>
                    <form class="forms-sample" id="form-edit-absence" action="{{ route('absence.update', $absence->ID) }}" method="post">
                        @csrf
                        {{ method_field('PATCH') }}
                        @include('partials.form-input', [
                          'title'       => __('Start Time'),
                          'type'        => 'text',
                          'name'        => 'StartTime',
                          'class'       => 'timepicker',
                          'required'    => true,
                          'value'       => $absence->StartTime
                        ])
                        @include('partials.form-input', [
                          'title'       => __('End Time'),
                          'type'        => 'text',
                          'name'        => 'EndTime',
                          'class'       => 'timepicker',
                          'required'    => true,
                          'value'       => $absence->EndTime
                        ])
                        @include('partials.form-input', [
                          'title'       => __('Status'),
                          'type'        => 'text',
                          'name'        => 'Status',
                          'required'    => true,
                          'value'       => $absence->Status
                        ])
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <a href="{{ route('absence.index') }}" class="btn btn-dark">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{!! JsValidator::formRequest('App\Http\Requests\AbsenceRequest', '#form-edit-absence') !!}
<script>
    $(document).ready(function() {
        // set navigation link active
        $('.nav-link[href="#master"]').parent().addClass('active');
        $('.link-absence').addClass('active')
        $('.link-absence').parent().parent().parent().addClass('show')

        $('.timepicker').pickatime({
            format: 'H:i'
        })
    });
</script>
@endsection