@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">Absence</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('partials.alert')
                    {{-- <div>
                        <a class="btn btn-primary btn-icon-text" href="{{ route('absence.create') }}">
                            <i class="mdi mdi-note-plus btn-icon-prepend"></i> 
                            Add Absence 
                        </a>
                    </div> --}}
                    <br />
                      <table class="table table-bordered" id="table">
                        <thead>
                          <tr>
                            <th> No </th>
                            <th> Start Time </th>
                            <th> End Time </th>
                            <th> Status </th>
                            <th> Action </th>
                          </tr>
                        </thead>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        // datatable
        var table = $('#table').DataTable({
         processing: true,
         serverSide: true,
         searching: false,
         ordering: false,
         ajax: '{{ route('absence.ajaxDatatable') }}',
         columns: [
             {
                "data": "id",
                "width": "5%",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                } 
             },
             {data: 'StartTime', name: 'Absence.StartTime', width: "10%"},
             {data: 'EndTime', name: 'Absence.EndTime', width: "10%"},
             {data: 'Status', name: 'Absence.Status', width: "15%"},
             {data: 'action', className: 'text-center', width: "10%"}
         ],
         "drawCallback": function(settings) {
         
          },            
             pageLength: 10,
         });
    });
</script>
@include('partials.datatable-delete', ['text' => 'Absence', 'table' => '#table'])
@endsection