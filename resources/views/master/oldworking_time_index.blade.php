@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="./assets/vendors/pickadate/themes/default.css">
<link rel="stylesheet" href="./assets/vendors/pickadate/themes/default.time.css">
<script src="./assets/vendors/pickadate/picker.js"></script>
<script src="./assets/vendors/pickadate/picker.time.js"></script>
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">Working Time</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="alert alert-success mt-4 d-flex align-items-center" role="alert" style="margin:20px 30px !important">
                    <i class="ti-info-alt"></i>
                    <p>
                        Set the start and end of working time in order to get employee status of presence. below start time then status will be "on time" but if more than start time start time will be "late"
                    </p>
                </div>
                @include('partials.alert')
                <div class="card-body">
                    <form class="forms-sample" id="form-edit-working-time" action="{{ route('working-time.update', $workingTime->WorkingTimeID) }}" method="post">
                        @csrf
                        {{ method_field('PATCH') }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Start Time</label>
                            <div class="input-group col-sm-6">
                                <input type="text" class="form-control timepicker" name="StartTime" value="{{ $workingTime->StartTime }}" />
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="mdi mdi-timetable"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">End Time</label>
                            <div class="input-group col-sm-6">
                                <input type="text" class="form-control timepicker" name="EndTime" value="{{ $workingTime->EndTime }}" />
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">
                                        <i class="mdi mdi-timetable"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <br />
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{!! JsValidator::formRequest('App\Http\Requests\WorkingTimeRequest', '#form-edit-working-time') !!}
<script>
    $('.timepicker').pickatime({
        format: 'H:i'
    })
</script>
@endsection