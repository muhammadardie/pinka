@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">Dashboard</h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-sm-4 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-9">
                            <div class="d-flex align-items-center align-self-start">
                                <h3 class="mb-0">{{ $totalEmp }}</h3>
                            </div>
                        </div>
                    </div>
                    <h6 class="text-muted font-weight-normal" style="margin-top: 20px">Total Employee</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-4 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-9">
                            <div class="d-flex align-items-center align-self-start">
                                <h3 class="mb-0">{{ $totalScanToday }}</h3>
                            </div>
                        </div>
                    </div>
                    <h6 class="text-muted font-weight-normal" style="margin-top: 20px">Total employee scanned today</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-4 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-9">
                            <div class="d-flex align-items-center align-self-start">
                                <h3 class="mb-0">{{ $conditionEmpToday['fever'] }}</h3>
                            </div>
                        </div>
                    </div>
                    <h6 class="text-muted font-weight-normal" style="margin-top: 20px">Total employee in fever condition today</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Temperature Today</h4>
                <canvas id="conditionEmpToday"></canvas>
              </div>
            </div>
        </div>
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Absence Today</h4>
                <canvas id="absenceToday"></canvas>
              </div>
            </div>
        </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Last week temperature</h4>
                <canvas id="latestTemp"></canvas>
              </div>
            </div>
        </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Last week absence</h4>
                <canvas id="latestAbsence"></canvas>
              </div>
            </div>
        </div>
    </div>
</div>
@include('dashboard-chart')
@endsection