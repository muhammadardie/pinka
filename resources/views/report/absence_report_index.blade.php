@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="page-header">
        <h3 class="page-title">Absence Report</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                              <label>Start date</label>
                                  <input type="text" class="form-control datepicker" name="startdate" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <label>End date</label>
                                  <input type="text" class="form-control datepicker" name="enddate" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <label>Employee</label>
                                  <select class="form-control select2" name="employee">
                                    <option value="all">-- All Employee --</option>
                                    @foreach($employees as $key => $employee)
                                        <option value="{{ $employee }}">{{ $employee }}</option>
                                    @endforeach
                                  </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <label>Status Absence</label>
                                  <select class="form-control select2" name="absence">
                                    <option value="all">-- All Status --</option>
                                    <option value="present">Presence or Late</option>
                                    <option value="leave">Leave or Overtime</option>
                                  </select>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="table-responsive">
                      <table class="table table-bordered" id="table">
                        <thead>
                          <tr>
                            <th> No </th>
                            <th> Record Time </th>
                            <th> Username </th>
                            <th> Department </th>
                            <th> Temperature </th>
                            <th> Absence </th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select2').select2().on('change', function() {
          table.ajax.reload();
        });

        // datepicker
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            autoclose: true,
            clearBtn: true,
        }).on('changeDate', function(e) {
           table.ajax.reload();
        });

        // datatable
        var table = $('#table').DataTable({
         processing: true,
         serverSide: true,
         searching: false,
         ordering: false,
         ajax: {
            url: '{{ route('absence-report.ajaxDatatable') }}',
            data: function (d) {
                d.startdate = $('input[name=startdate]').val();
                d.enddate   = $('input[name=enddate]').val();
                d.employee  = $('select[name=employee]').val();
                d.absence   = $('select[name=absence]').val();
                d.log       = $('select[name=log]').val();
            }
         },
         columns: [
             {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                } 
             },
             {data: 'RecordTime', name: 'RecordLog.RecordTime'},
             {data: 'UserName', name: 'UserInfo.UserName'},
             {data: 'department', name: 'UserGroup.GroupName'},
             {data: 'Temperature', name: 'RecordLog.Temperature'},
             {data: 'absence'}
         ],
         "drawCallback": function(settings) {
         
          },            
             pageLength: 10,
         });

    });
</script>
@endsection