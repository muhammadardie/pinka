<script>
	$(function() {

    window.chartColors = {
      red: 'rgb(255, 99, 132)',
      orange: 'rgb(255, 159, 64)',
      yellow: 'rgb(255, 205, 86)',
      green: 'rgb(75, 192, 192)',
      blue: 'rgb(54, 162, 235)',
      purple: 'rgb(153, 102, 255)',
      grey: 'rgb(201, 203, 207)'
    };


  if ($("#latestTemp").length) {
    var latestTempData = {
      labels: @json($lastWeekdays),
      datasets: [
      {
        label: 'Minimal Temperature',
        data: @json($temperature['min']),
        borderColor: window.chartColors.blue,
        backgroundColor: window.chartColors.blue,
        fill: false,
      },
      {
        label: 'Average Temperature',
        data: @json($temperature['avg']),
        borderColor: window.chartColors.yellow,
        backgroundColor: window.chartColors.yellow,
        fill: false,
      },
      {
        label: 'Max Temperature',
        data: @json($temperature['max']),
        borderColor: window.chartColors.red,
        backgroundColor: window.chartColors.red,
        fill: false,
      },
      ]
    };
  
    var latestTempOpt = {
      plugins: {
        filler: {
          propagate: true
        }
      },
      scales: {
        yAxes: [{
          gridLines: {
            color: "rgba(204, 204, 204,0.1)"
          },
          ticks: {
            min: 34,
            stepSize: 0.2,
          }
        }],
        xAxes: [{
          gridLines: {
            color: "rgba(204, 204, 204,0.1)"
          }
        }]
      }
    }
    var latestTempCanvas = $("#latestTemp").get(0).getContext("2d");
    var latestTemp = new Chart(latestTempCanvas, {
      type: 'bar',
      data: latestTempData,
      options: latestTempOpt
    });

  }

  if ($("#latestAbsence").length) {
    var latestAbsenceData = {
      labels: @json($lastWeekdays),
      datasets: [
      {
        label: 'Total Present',
        data: @json($presence['present']),
        borderColor: window.chartColors.blue,
        backgroundColor: window.chartColors.blue,
        fill: false,
      },
      {
        label: 'Total Late',
        data: @json($presence['late']),
        borderColor: window.chartColors.yellow,
        backgroundColor: window.chartColors.yellow,
        fill: false,
      },
      {
        label: 'Total Excuse',
        data: @json($presence['excuse']),
        borderColor: window.chartColors.red,
        backgroundColor: window.chartColors.red,
        fill: false,
      },
      ]
    };
  
    var latestAbsenceOpt = {
      plugins: {
        filler: {
          propagate: true
        }
      },
      scales: {
        yAxes: [{
          gridLines: {
            color: "rgba(204, 204, 204,0.1)"
          },
          ticks: {
            min: 0,
          }
        }],
        xAxes: [{
          gridLines: {
            color: "rgba(204, 204, 204,0.1)"
          }
        }]
      }
    }
    var latestAbsenceCanvas = $("#latestAbsence").get(0).getContext("2d");
    var latestAbsence = new Chart(latestAbsenceCanvas, {
      type: 'bar',
      data: latestAbsenceData,
      options: latestAbsenceOpt
    });

  }

  if ($("#conditionEmpToday").length) {
    var conditionEmpTodayData = {
      labels: ['Normal', 'Fever'],
      datasets: [
      {
        data: [@json($conditionEmpToday['normal']),@json($conditionEmpToday['fever'])],
        borderColor: ['rgba(54, 162, 235, 1)', 'rgba(255,99,132,1)'],
        backgroundColor: ['rgba(54, 162, 235, 0.5)', 'rgba(255, 99, 132, 0.5)'],
      }
      ]
    };
  
    var conditionEmpTodayOpt = {
      responsive: true,
      animation: {
        animateScale: true,
        animateRotate: true
      }
    }
    var conditionEmpTodayCanvas = $("#conditionEmpToday").get(0).getContext("2d");
    var conditionEmpToday = new Chart(conditionEmpTodayCanvas, {
      type: 'doughnut',
      data: conditionEmpTodayData,
      options: conditionEmpTodayOpt
    });

  }

  if ($("#absenceToday").length) {
    var absenceTodayData = {
      labels: ['Present', 'Late', 'Excuse'],
      datasets: [
      {
        data: [@json($absenceToday['present'][0]),@json($absenceToday['late'][0]),@json($absenceToday['excuse'][0])],
        borderColor: ['rgba(54, 162, 235, 1)', 'rgba(255,99,132,1)', 'rgba(255, 206, 86, 0.5)'],
        backgroundColor: ['rgba(54, 162, 235, 0.5)', 'rgba(255, 99, 132, 0.5)', 'rgba(255, 206, 86, 0.5)'],
      }
      ]
    };
  
    var absenceTodayOpt = {
      responsive: true,
      animation: {
        animateScale: true,
        animateRotate: true
      }
    }
    var absenceTodayCanvas = $("#absenceToday").get(0).getContext("2d");
    var absenceToday = new Chart(absenceTodayCanvas, {
      type: 'pie',
      data: absenceTodayData,
      options: absenceTodayOpt
    });

  }


});
</script>