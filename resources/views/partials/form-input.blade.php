<div class="form-group row">
    <label class="col-sm-2 col-form-label">
        @if( isset($required) )
            <span style="color:red">*</span>
        @endif
        {{ $title }}
    </label>
    <div class="col-sm-6">
        <input  
            type="{{ isset($type) ? $type : 'text' }}" 
            class="form-control {{ isset($class) ? $class : '' }}"
            name="{{ $name }}"
            value="{{ isset($value) ? $value : '' }}"
            placeholder="{{ $title }}"

            {{ (isset($attribute) ? is_array($attribute) : false) ? implode(' ',$attribute) : ''}}
        >
    </div>

</div>

@if( isset($date) )
    <script>
        let input = $('input[name={{ $name }}]'); 
        <?= \Helper::date_formats('$(input)', 'js') ?>
    </script>
@endif