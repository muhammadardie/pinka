<div class="form-group row">
    <label class="col-sm-2 col-form-label">
        @if( isset($required) )
            <span style="color:red">*</span>
        @endif
        {{ $title }}
    </label>

    <div class="col-sm-6">
        <select 
          name="{{ $name }}" 
          class="form-control"
          {{ (isset($attribute) ? is_array($attribute) : false) ? implode(' ',$attribute) : ''}}
        >

        <option></option>
          @foreach($data as $key => $value)
              <option value="{{ $key }}" 
              @if(isset($selected) && !is_array($selected) && $selected== $key)
                selected
              @endif
              > 
                {!! $value !!} 
              </option>
          @endforeach
      </select>
    </div>
</div>

<script>
  $("select[name='{{ $name }}']").select2({ 
    placeholder: "{{ \Lang::get('-- Select '. $title . ' --') }}",
    width: '100%'
  });
</script>