@if(in_array('create', $availablePermission))
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
           <a href="{{ $route }}" class="btn btn-brand btn-icon-sm"> 
            <i class="flaticon2-plus"></i> 
            {{ $text }} 
           </a>
        </div>
    </div>
@endif