@if(Session::has('alert'))

	<div id="alert-status" class="alert {{ Session::get('alert') }} mt-2 d-flex align-items-center" role="alert">
      <p>{{ Session::get('alert-text') }}</p>
    </div>

    <script>
        $("#alert-status").fadeTo(3000, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    </script>
@endif