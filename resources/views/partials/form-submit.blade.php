<div class="kt-form__actions">
    <div class="col-lg-6">
    	@if( !isset($noSubmit) )
	      <button type="submit" class="btn btn-primary btn-submit">Simpan</button>
	      &nbsp;
	    @endif
      <a href="#" class="btn btn-secondary btn-kembali">Kembali</a>
    </div>
</div>

<script>
$(document).ready(function() { 

	@if( !isset($noSubmit) )
		
		// on click enter auto submit
	    $(document).keypress(function (e) {
		  if (e.which == 13) {
		    $('form.kt-form').submit();
		    return false;
		  }
		});

		// on click submit disable button submit and process
	    $('form.kt-form').on('submit', function() {
	        $(this).valid() && disableButton();
		});

    @endif
    // on click back button redirect to index menu page
    $('a.btn-kembali').on('click', backToIndex);

    function disableButton(){
		$('.btn-submit').addClass('kt-spinner kt-spinner--sm kt-spinner--light');
		$('.btn-submit').attr('disabled', true);
	}

	function backToIndex(e){
		e.preventDefault();
		let redirect    = $('li.kt-menu__item--active').children().attr('href');
		window.location = redirect;
	}

}); 
</script>