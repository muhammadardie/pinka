@extends('layouts.app')
@section('content')
<style>
  .blink{
    animation:blinking 1.5s infinite;
    color:green;
  }
  @keyframes blinking{
    0%{   color: red;   }
    47%{   color: #000; }
    62%{   color: transparent; }
    97%{   color:transparent; }
    100%{  color: #000;   }
  }
  #colon { visibility:hidden }
  .scan-info {
    margin: 10px 180px;
  }
  .time-scan {
    font-size: 3rem;
  }
  .temperature-scan {
    font-size: 2rem;
  }
  .text-green {
    color: #27ff00 !important;
  }
</style>
<div class="content-wrapper">
    <div class="page-header header-scan text-center" style="float:left">
        <div class="form-group scan-info">
          <label class="scan-label">Time
          </label>
          <br />
          <div class="time-scan text-green">
            <span id="hours"></span><span id="colon">:</span><span id="minutes"></span>
          </div>
        </div>
        <div class="form-group scan-info">
          <label class="scan-label">Normal Temperature
          </label>
          <br />
          <div class="temperature-scan text-green">
            {{ $temperature }} °C and below
          </div>
        </div>
    </div>
    <div style="clear:both;"></div>
    <div class="row">
        <div class="col-md-5 scan-face">
          <img id="default-scan-face-img" src="./assets/images/live-scan.gif" />

          <div id="scan-face-frame" class="none">
            <img id="scan-face-img" />
          </div>

        </div>
        <div class="col-md-7 scan-detail">
            <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <form class="forms-sample">
                        <div class="form-group">
                          <label class="scan-label">Username</label>
                          <input type="text" class="form-control scan-input" placeholder="-" name="username" disabled>
                        </div>
                        <div class="form-group">
                          <label class="scan-label">Department</label>
                          <input type="email" class="form-control scan-input" placeholder="-" name="department" disabled>
                        </div>
                        <div class="form-group">
                          <label class="scan-label">Temperature</label>
                          <input type="text" class="form-control scan-input" placeholder="-" name="temperature" disabled>
                        </div>
                      </form>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label class="scan-label">Time</label>
                          <input type="text" class="form-control scan-input" placeholder="-" name="time" disabled>
                        </div>
                        <div class="form-group">
                          <label class="scan-label">Condition</label>
                          <input type="text" class="form-control scan-input" placeholder="-" name="condition" disabled>
                        </div>
                        <div class="form-group">
                          <label class="scan-label">Presence</label>
                          <input type="text" class="form-control scan-input" placeholder="-" name="presence" disabled>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('monitoring.live_scan_script')
@endsection