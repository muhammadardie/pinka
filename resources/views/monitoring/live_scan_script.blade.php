<script>
jQuery(document).ready(function() {

	var toggle = true;
	setInterval(function() {
	  var d = new Date().toLocaleTimeString('en-US', { hour12: false, hour: 'numeric', minute: 'numeric' });
	  var parts = d.split(":");
	  $('#hours').text(parts[0]);
	  $('#minutes').text(parts[1]);
	  $("#colon").css({ visibility: toggle?"visible":"hidden"});
	  toggle=!toggle;
	},1000);



	$('.navbar-toggler[data-toggle="minimize"]')[0].click();

	var recordTime     = 0;
	var resetCounter   = 0;
	var resetCount     = 3;
	let scanFaceImg    = $('#scan-face-img').attr('src');
	let defaultScanImg = $('#default-scan-face-img');
	let scanImgFrame   = $('#scan-face-frame');
	let scanImg        = $('#scan-face-img');

	// do live scan every 60 second
	setTimeout(liveScan, 2000);

	function liveScan() {
	    $.ajax({
	        url: '{{ route('live-scan-monitor') }}', //this is your uri
	        type: 'GET', //this is your method
	        dataType: 'json',
	        success: function(res){
	        	console.log(res)
	        	if(res.length == 0 || res.RecordTime == recordTime) {
	        		resetDisplay()
	        		return;
	        	}
	        	setDisplay(res)
	
	        },
	        error: function(err) {
	          	console.log(err)
	        }
	    });

	    setTimeout(liveScan, 2000);
	}

	function resetDisplay() {
	    resetCounter += 1;
	    if(resetCounter == resetCount) {
	    	$('input[name=username]').val('-')
	    	$('input[name=department]').val('-')
	    	$('input[name=temperature]').val('-')
	    	$('input[name=time]').val('-')
	    	$('input[name=condition]').val('-')
	    	$('input[name=condition]').css({"color": 'white'})
	    	$('input[name=presence]').val('-')
	    	$('input[name=presence]').css({"color": 'white'})
	    	
	    	scanImgFrame.addClass('none')
	    	defaultScanImg.removeClass('none')
	    }
	}

	function setDisplay(res) {
    	resetCounter = 0; // reset counter
    	recordTime   = res.RecordTime // set record time scanned at this time
    	let condColor = res.condition == 'normal' ? '#27ff00' : 'red';
    	let presenceColor = res.presence == 'late' ? 'red' : '#27ff00';

    	$('input[name=username]').val(res.username)
    	$('input[name=department]').val(res.department)
    	$('input[name=temperature]').val(res.Temperature)
    	$('input[name=time]').val(res.time)
    	$('input[name=condition]').val(res.condition)
    	$('input[name=condition]').css({"color": condColor})
    	$('input[name=presence]').val(res.presence)
    	$('input[name=presence]').css({"color": presenceColor})

    	defaultScanImg.addClass('none') // hide default img
    	scanImg.attr('src', res.PicPath)
    	scanImgFrame.removeClass('none') // display default img    	
	}

});
</script>