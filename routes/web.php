<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|c
*/

// for generate dummy record log
// Route::get('query', 'SeederController@dummyScan');

Route::get('/', function () {
    return redirect('dashboard');
});

Route::group(['middleware' => ['auth', 'web']], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    // USER MANAGEMENT
    Route::prefix('user-management')->group(function () {
        // employee
        Route::get('/employee/ajaxDatatable', 'UserManagement\EmployeeController@ajaxDatatable')->name('employee.ajaxDatatable');
        Route::resource('employee', 'UserManagement\EmployeeController', ['names' => 'employee']);

        // user-account
        Route::get('/user-account/ajaxDatatable', 'UserManagement\UserAccountController@ajaxDatatable')->name('user-account.ajaxDatatable');
        Route::resource('user-account', 'UserManagement\UserAccountController', ['names' => 'user-account']);
    });

    // MASTER
    Route::prefix('master')->group(function () {
        // fever
        Route::resource('fever', 'Master\FeverController', ['names' => 'fever']);
        // absence
        Route::get('/absence/ajaxDatatable', 'Master\AbsenceController@ajaxDatatable')->name('absence.ajaxDatatable');
        Route::resource('absence', 'Master\AbsenceController', ['names' => 'absence']);
    });

    // MONITORING
    Route::prefix('monitoring')->group(function () {
        Route::get('/live-scan', 'Monitoring\LiveScanController@index')->name('live-scan');
        Route::get('/live-scan-monitor', 'Monitoring\LiveScanController@monitor')->name('live-scan-monitor');
    });

    // REPORT
    Route::prefix('report')->group(function () {
        // scanning report
        Route::get('/scanning-report/ajaxDatatable', 'Report\ScanningReportController@ajaxDatatable')->name('scanning-report.ajaxDatatable');
        Route::resource('scanning-report', 'Report\ScanningReportController', ['names' => 'scanning-report']);

        // absence report
        Route::get('/absence-report/ajaxDatatable', 'Report\AbsenceReportController@ajaxDatatable')->name('absence-report.ajaxDatatable');
        Route::resource('absence-report', 'Report\AbsenceReportController', ['names' => 'absence-report']);
    });
});

Auth::routes();
