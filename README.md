# PINKA
Face Recognition app (PINdai muKA)

## Requirement ##
- MS Access Database Engine Installed
- PHP 7.2 or above
- FaceServerManager has been setup and running in folder /public/FaceServerManager

## Installation ##

* `git clone https://muhammadardie@bitbucket.org/muhammadardie/pinka.git`
* `cd pinka`
* `composer install`
* `cp .env.example .env`
* `php artisan key:generate`
* Setup `odbc` connection in `config/database.php`
* Setup `facemanager` config  in `config/facemanager.php`

## Login

email: admin@localhost.com
password: 123456